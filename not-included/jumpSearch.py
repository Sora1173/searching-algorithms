import math

items = [7, 9, 13, 19, 39, 40, 61, 81, 84, 87]
target = 40
n = len(items)

def jumpSearch(items, target, n):
    start = 0
    jump = math.floor(math.sqrt(n))
    end = jump

    while items[end] <= target:
        start = end
        end += jump

        if end >= n:
            end = n - 1
        
        if start >= n:
            return None
    
    for item in items:
        if item == target:
            return items.index(item)
    
    return None

print("index: " + str(jumpSearch(items, target, n)))
