items = [1,0,2,9,3,8,4,7,5,6]
n = len(items)

def selectionSort(items, n):
    for i in range(n - 1):
        m = i
        for j in range(i + 1, n):
            if items[m] > items[j]:
                m = j
        if m != i:
            (items[m], items[i]) = (items[i], items[m])
    return items

items = selectionSort(items, n)
print(items)
