items = [1,0,2,9,3,8,4,7,5,6]
n = len(items)

def bubbleSort(items, n):
    for i in range(n - 1):
        for j in range(n - 1):
            if items[j] > items[j + 1]:
                (items[j], items[j + 1]) = (items[j + 1], items[j])
    return items

items = bubbleSort(items, n)
print(items)
