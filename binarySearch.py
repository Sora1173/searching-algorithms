
items = [7, 9, 13, 19, 39, 40, 61, 81, 84, 87]
target = 81
n = len(items)

def binarySearch(items, target, n):
    start = 0
    end = n - 1

    while start <= end:
        mid = round((start + end) / 2)

        if items[mid] > target:
            end = mid - 1
        elif items[mid] < target:
            start = mid + 1
        else:
            return items.index(items[mid])
    return None

print("index: " + str(binarySearch(items, target, n)))
